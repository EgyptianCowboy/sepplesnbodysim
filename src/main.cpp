//#include "spdlog/spdlog.h"
//#include "spdlog/sinks/stdout_color_sinks.h"
//#include <array>
//#include <chrono>
//#include <fmt/format.h>
//#include <vector>

#include "board.hpp"
#include "body.hpp"
#include "coor.hpp"
#include "simHelper.hpp"
#include "sim.hpp"

int main() {
    // auto console = spdlog::stdout_color_mt("console");
    // auto err_logger = spdlog::stderr_color_mt("stderr")

    sim::sim<double> bodySim;
    bodySim.start();
    return 0;
}
