#ifndef LBODY
#define LBODY
#include <list>
#include <chrono>
#include <fmt/printf.h>
#include <memory>
#include "body.hpp"
#include "simHelper.hpp"

namespace simElem {
    template<typename T>
    class lBody : public body<T> {
    public:
	/////////////////
	// Constructor //
	/////////////////
	lBody(T xPos, T yPos, const T mass, const T diameter, const std::string name)
	    : body<T>(xPos, yPos, mass, diameter, name) {}
	lBody(T xPos, T yPos, const T mass, const T diameter, T xvel, T yvel, const std::string name)
	    : body<T>(xPos, yPos, mass, diameter, xvel, yvel, name) {}

	//////////////////////
	// Functions	    //
	//////////////////////
	// constexpr auto calcBodyInf([[maybe_unused]] std::unique_ptr<simElem::body<T>> oppBody) noexcept -> void {}

	constexpr auto calcBodyInf([[maybe_unused]] simElem::body<T>& oppBody) noexcept -> void {}

	constexpr auto newPos(std::chrono::duration<T> elTime) noexcept -> void {
	    this->addVelX(this->getForceX() / this->getMass() * elTime.count());
	    this->addVelY(this->getForceY() / this->getMass() * elTime.count());
	    this->setXPos(this->getXPos() + this->getXVel() * elTime.count());
	    this->setYPos(this->getYPos() + this->getYVel() * elTime.count());
	    this->setForceX(0);
	    this->setForceY(0);
	}

	[[nodiscard]] constexpr auto boundCalc() noexcept -> bool {
	    return ((this->getXPos() > simHelper::BOARDWIDTH<T> || this->getXPos() < 0) ||
		    (this->getYPos() > simHelper::BOARDHEIGHT<T> || this->getYPos() < 0));
	}
    private:
    };
}
#endif
