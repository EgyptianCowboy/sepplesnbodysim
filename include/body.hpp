#ifndef BODY
#define BODY

#include <chrono>
#include <memory>
#include <string>
#include <fmt/printf.h>
#include "coor.hpp"
#include "simHelper.hpp"
#define _USE_MATH_DEFINES

namespace simElem {

    /////////////////////////////////////////////////////////////////////////////////////////
    // Body class is used to simulate bodies with mass and diameter			   //
    // Constructor initial conditions: xPos is the x-coordinate, yPos is the y-coordinate  //
    // xvel and yvel are the initial velocities                                            //
    /////////////////////////////////////////////////////////////////////////////////////////

    template<typename T>
    class body : public coor<T> {
    public:
	////////////////////////////////
	// Constructor and destructor //
	////////////////////////////////
	body(const T xPos, const T yPos, const T mass, const T diameter, const std::string name) noexcept
	    : coor<T>(xPos, yPos), properties{mass, diameter, std::move(name)}, vel{0, 0}, forces{0, 0} {}
	body(const T xPos, const T yPos, const T mass, const T diameter, const T xvel, const T yvel, const std::string name) noexcept
	    : coor<T>(xPos, yPos), properties{mass, diameter, std::move(name)}, vel{xvel, yvel},  forces{0, 0} {}
	virtual ~body() {}

	//////////////////////
	// Functions	    //
	//////////////////////

	[[nodiscard]] constexpr auto density() const noexcept -> T {
	    return (getMass() / ((4 / 3) * M_PI * std::pow(getDiameter(), 3)));
	}

	// DistX has to be xpos mass2 - xpos mass1
	constexpr auto addForceX(const T oppMass, const T distX, const T distSum = 1) noexcept -> void{
	    setForceX(getForceX()+(simHelper::SCALE<T>*simHelper::GRAVCONST<T>*distX*((this->getMass()*oppMass)/(pow(distSum,3)))));
	}
	constexpr auto addForceX(const T oppForce = 0) const noexcept -> void {
	    setForceX(getForceX()-oppForce);
	}

	// DistY has to be ypos mass2 - yposs mass2
	constexpr auto addForceY(const T oppMass, const T distY, const T distSum = 1) noexcept -> void{
	    setForceY(getForceY()+(simHelper::SCALE<T>*simHelper::GRAVCONST<T>*distY*((this->getMass()*oppMass)/(pow(distSum,3)))));
	}
	constexpr auto addForceY(const T oppForce = 0) const noexcept -> void{
	    setForceY(getForceY()-oppForce);
	}


	constexpr auto addVelX(const T xPos = 0) noexcept -> void{
	    vel.XVel+=xPos;
	}
	constexpr auto addVelY(const T yPos = 0) noexcept -> void{
	    vel.YVel+=yPos;
	}

	[[nodiscard]] constexpr auto collision(const std::unique_ptr<simElem::body<T>>& oppBody) noexcept -> bool{
	    fmt::print("Test\n");
	    return true;
		//((getDiameter()/2+oppBody->getDiameter()/2) < (this->distFromPoint(oppBody->getXPos(), oppBody->getYPos())));
	}

	//////////////////////////////
	// Virtual functions	    //
	//////////////////////////////

	virtual auto calcBodyInf([[maybe_unused]] simElem::body<T>& oppBody) noexcept -> void = 0;
	virtual auto newPos(std::chrono::duration<T> elTime) noexcept -> void= 0;
	[[nodiscard]] virtual auto boundCalc() noexcept -> bool = 0;

	//////////////////////
	// Operators	    //
	//////////////////////

	body<T>& operator < (const body<T>& oppBody) {
	    fmt::print("Testop\n");
	    return density() < oppBody->density();
	}

	body<T>& operator > (const body<T>& oppBody) {
	    fmt::print("Testop\n");
	    return density() > oppBody->density();
	}

	body<T>& operator == (const body<T>& oppBody) {
	    fmt::print("Testop\n");
	    return density() == oppBody->density();
	}

	/////////////
	// Setters //
	/////////////

	constexpr auto setXVel(const T xPos) noexcept -> void {
	    vel.XVel = xPos;
	}
	constexpr auto setYVel(const T yPos) noexcept -> void {
	    vel.YVel = yPos;
	}
	constexpr auto setForceX(const T forceX) noexcept -> void{
	    forces.forceX = forceX;
	}
	constexpr auto setForceY(const T forceY) noexcept -> void{
	    forces.forceY = forceY;
	}

	/////////////
	// Getters //
	/////////////

	[[nodiscard]] constexpr auto getMass() const noexcept -> T {
	    return properties.mass;
	}
	[[nodiscard]] constexpr auto getDiameter() const noexcept -> T {
	    return properties.diameter;
	}
	[[nodiscard]] constexpr auto getXVel() const noexcept -> T {
	    return vel.XVel;
	}
	[[nodiscard]] constexpr auto getYVel() const noexcept -> T {
	    return vel.YVel;
	}
	[[nodiscard]] constexpr auto getForceX() const noexcept -> T {
	    return forces.forceX;
	}
	[[nodiscard]] constexpr auto getForceY() const noexcept -> T {
	    return forces.forceY;
	}
	[[nodiscard]] auto getName() const noexcept -> std::string {
	    return properties.name;
	}

    private:
	struct properties_t {
	    const T mass;
	    const T diameter;
	    const std::string name;
	} properties;
	struct vel_t {
	    T XVel;
	    T YVel;
	} vel;
	struct forces_t {
	    T forceX;
	    T forceY;
	} forces;
    };
}

#endif
