#ifndef SIMH
#define SIMH

#include <vector>
#include <memory>
#include <chrono>
#include <string>
#include "simHelper.hpp"
#include "bodyFactory.hpp"
#include "body.hpp"
#include "board.hpp"

namespace sim {
    template<
	typename T,
	typename timeUnit = std::chrono::seconds,
	typename clock = std::chrono::system_clock
	>
    class sim {
    public:
	sim() : frameTime {0},
		simBoard(simHelper::BOARDWIDTH<T>, simHelper::BOARDHEIGHT<T>, 0, 0),
		bodyPtr(nullptr), bodyVec(),
		simFactory(), lastTime{clock::duration::zero()},
		currentTime{clock::duration::zero()},
		elapsedTime{clock::duration::zero()} {}
	~sim() {}

	constexpr auto start() noexcept -> void {
	    initialize();
	    _running = true;
	    setFrameTime(0.5);
	    runSim();
	    run();
	}

    private:
	constexpr auto initialize () noexcept -> void {
	    fmt::print("Board dimensions: X: {} Y: {}\n\n", simBoard.getWidth(),simBoard.getHeight());

	    if((bodyPtr=simFactory.createBody(500, 400, 1e8, 100, "Hmm")))
		bodyVec.push_back(std::move(bodyPtr));

	    if((bodyPtr=simFactory.createBody(50, 50, 1e10, 100, "Big boy")))
		bodyVec.push_back(std::move(bodyPtr));

	    if((bodyPtr=simFactory.createBody(200, 10, 1e15, 100, "Bigger boy")))
		bodyVec.push_back(std::move(bodyPtr));

	    //if((bodyPtr=simFactory.createBody(simBoard.getZeroX() + 50, simBoard.getZeroY() + 50, 1e20, 1000, "Biggest")))
	    //bodyVec.push_back(std::move(bodyPtr));

	    if((bodyPtr=simFactory.createBody(1, 1, -1, 1000, std::to_string(bodyVec.size()))))
		bodyVec.push_back(std::move(bodyPtr));

	    if(bodyVec[0]<bodyVec[1])
		fmt::print("Testing overloading\n\n");

	    fmt::print("\nSize of bodyList {}\n",bodyVec.size());
	}

	[[nodiscard]] constexpr auto getFrameTime() const noexcept -> float {
	    return frameTime;
	}

	constexpr auto setFrameTime(float frameT) noexcept -> void {
	    frameTime = frameT;
	}

	constexpr auto shutdown() noexcept -> void {
	    _running = false;
	    _exit = true;
	}

	constexpr auto pause() noexcept -> void {
	    _paused = true;
	    _stopped = false;
	    _runSim = false;
	}

	constexpr auto stop() noexcept -> void {
	    bodyVec.erase();
	    _stopped = true;
	    _runSim = false;
	}

	constexpr auto runSim() noexcept -> void {
	    _runSim = true;
	    _stopped = false;
	    _exit = false;
	}

	constexpr auto run() noexcept -> void {
	    lastTime = std::chrono::system_clock::now();
	    while (_running) {
		while (_runSim) {
		    currentTime = std::chrono::system_clock::now();
		    elapsedTime = currentTime - lastTime;
		    //fmt::print("Frame time = {}\n", elapsedTime.count());
		    if (elapsedTime.count() > static_cast<double>(getFrameTime())) {
			fmt::print("Frame time = {}\n", elapsedTime.count());
			frame();
			fmt::print("\n");
			lastTime = currentTime;
		    }
		}
		while(_paused) {
		}
		while(_stopped) {
		}
	    }
	    shutdown();
	}

	constexpr auto frame() noexcept -> void {
	    for(const auto& vi : bodyVec) {
		fmt::print("Element {}, xpos: {} ypos: {}, xvel: {}, yvel: {} \n",
			   vi->getName(), vi->getXPos(), vi->getYPos(), vi->getXVel(), vi->getYVel());
		for (const auto &vj : bodyVec) {
		    if (&vi != &vj)
			vi->calcBodyInf(*vj);
		}
	    }

	    // Out of bound checking
	    for(auto vi=bodyVec.begin(); vi!=bodyVec.end();) {
		(*vi)->newPos(elapsedTime);
		if((*vi)->boundCalc()) {
		    fmt::print("Out of bounds, erasing element.\n\n");
		    vi=bodyVec.erase(vi);
		} else
		    vi++;
	    }

	    // Collision checking
	    /*for(auto vi=bodyVec.begin(); vi!=bodyVec.end();) {
		for (auto vj=bodyVec.begin(); vj!=bodyVec.end();) {
		    fmt::print("test\n");
		    if (*(&vi) != *(&vj)) {
			if((*vi)->collision(*vj)) {
			    fmt::print("Collision, deleting an element.\n");
			    fmt::print("Element {} and {}\n", (*vi)->getName(), (*vj)->getName());
			    if (&(*vi) < &(*vj)) {
				fmt::print("vi<vj\n\n");
				vi = bodyVec.erase(vi);
				vj++;
			    }
			    else if (&(*vi) > &(*vj)) {
				fmt::print("vi>vj\n\n");
				vj = bodyVec.erase(vj);
				vi++;
			    }
			    else {
				fmt::print("vi==vj\n\n");
				vi = bodyVec.erase(vi);
				vj = bodyVec.erase(vj);
			    }
			}
		    } else {
			vj++;
			if (vj == bodyVec.end()) {
			    vi++;
			}
		    }
		}
		}*/
	}

	bool _running = false;
	bool _paused = false;
	bool _stopped = false;
	bool _exit = false;
	bool _runSim = false;
	float frameTime;
	const simElem::board<T> simBoard;
	std::unique_ptr<simElem::body<T>> bodyPtr;
	std::vector<std::unique_ptr<simElem::body<T>>> bodyVec;
	bodyFactory::bodyFactory<T> simFactory;
	std::chrono::time_point<clock> lastTime;
	std::chrono::time_point<clock> currentTime;
	std::chrono::duration<T> elapsedTime;
    };
}

#endif
