#ifndef BODYMANAGER
#define BODYMANAGER

#include <tuple>
#include <vector>
#include <memory>
#include <string>
#include "bodyFactory.hpp"
#include "body.hpp"

namespace bodyManager {
    template<typename T>
    class bodyManager {
    public:
	//////////////////////
	// Functions	    //
	//////////////////////

	[[nodiscard]] constexpr static auto instance() noexcept -> bodyManager<T>* {
	    if (_instance == nullptr) {
		fmt::print("Implementing manager\n");
		_instance = new bodyManager<T>;
	    }
	    return _instance;
	}

	// [[nodiscard]] constexpr auto FacInstance() const noexcept -> std::unique_ptr<bodyFactory::bodyFactory<T>> {
	//     return _factoryInstance;
	// }

	constexpr auto insertBody(const T x, const T y, const T mass, const T diameter, const T xvel = 0, const T yvel = 0) const noexcept -> void {
	    std::unique_ptr<simElem::body<T>> bodyPtr = nullptr;
	    if((_instance->createBody(x,y,mass,diameter,xvel,yvel)))
		bodyVec.push_back(std::move(bodyPtr));
	}

	constexpr auto print() const noexcept -> void {
	    fmt::print("HMM\n");
	}

	constexpr bodyManager(bodyManager const&) noexcept = delete;
	constexpr bodyManager(bodyManager const&&) noexcept = delete;
	constexpr bodyManager& operator=(bodyManager const&) const noexcept = delete;
	constexpr bodyManager& operator=(bodyManager const&&) const noexcept = delete;

	//bodyFactory::bodyFactory<T>::createBody(const T x, const T y, const T mass, const T diameter);

	////////////////////
	// Getters	  //
	////////////////////

	[[nodiscard]] constexpr auto getBodyVec() const noexcept -> std::vector<std::unique_ptr<simElem::body<T>>> {
	    return this->bodyVec;
	}

      protected:
      private:
	bodyManager() {
	    //_factoryInstance = bodyFactory::bodyFactory<T>::instance();
	}

	~bodyManager() {
	    delete _instance;
	}

	friend bodyFactory::bodyFactory<T>;
	static std::vector<std::unique_ptr<simElem::body<T>>> bodyVec;
	//const static std::unique_ptr<bodyFactory::bodyFactory<T>> _factoryInstance = nullptr;
	static bodyManager<T>* _instance;
    };
}
#endif
